#include "Monitor.h"
#include "sgx_dh.h"
#include <string>
#include <stdio.h> 
#include "sgx_tcrypto.h"

uint8_t internalState = 0; 

uint32_t random;
sgx_sha256_hash_t hashRandom;

sgx_ecc_state_handle_t eccContext = NULL; 
sgx_ec256_public_t publicKey;
sgx_ec256_private_t privateKey;
sgx_ec256_signature_t mySignature;

sgx_sha256_hash_t* hashList = NULL;
uint32_t* randomList = NULL;
sgx_ec256_signature_t* signatureList=NULL; 
sgx_ec256_public_t* pubKeyList=NULL;

sgx_ec256_public_t ChoosenpubKey;
sgx_key_128bit_t dh_aek;
sgx_key_128bit_t* dh_aek_succ;

uint32_t Xrandom;
sgx_ec256_signature_t XrandomSign;


sgx_status_t sgx_ecdsa_sign_uint32(uint32_t block,size_t blocksize, sgx_ec256_private_t* privateKey,
sgx_ec256_signature_t* mySignature,sgx_ecc_state_handle_t eccContext)
{	
	uint8_t* signablerandom=(uint8_t*)malloc(blocksize);
	memcpy(signablerandom,&block,blocksize);
	return sgx_ecdsa_sign(signablerandom, blocksize, privateKey, mySignature, eccContext);
	
}
sgx_status_t sgx_ecdsa_verify_uint32(uint32_t block,size_t blocksize, sgx_ec256_public_t* publicKey,
sgx_ec256_signature_t* mySignature,uint8_t*  result,sgx_ecc_state_handle_t eccContext)
{	
	uint8_t* signablerandom=(uint8_t*)malloc(blocksize);
	memcpy(signablerandom,&block,blocksize);
	return sgx_ecdsa_verify(signablerandom, sizeof(uint32_t), publicKey, mySignature, result, eccContext);
	
}



#define BUFLEN 4096
sgx_status_t decryptMessage(sgx_key_128bit_t* key,char *encyptedMessage, size_t len, char *decMessageOut, size_t lenOut)
{
	uint8_t *encMessage = (uint8_t *) encyptedMessage;
	uint8_t p_dst[BUFLEN] = {0};
	sgx_status_t internalStatus=SGX_SUCCESS;
	internalStatus=sgx_rijndael128GCM_decrypt(
		key,
		encMessage + SGX_AESGCM_MAC_SIZE + SGX_AESGCM_IV_SIZE,
		lenOut,
		p_dst,
		encMessage + SGX_AESGCM_MAC_SIZE, 
		SGX_AESGCM_IV_SIZE,
		NULL, 0,
		(sgx_aes_gcm_128bit_tag_t *) encMessage);
	memcpy(decMessageOut, p_dst, lenOut);
	return internalStatus;
}

sgx_status_t encryptMessage(sgx_key_128bit_t* key,char *decMessageIn, size_t len, char *encMessageOut, size_t lenOut)
{
	uint8_t *origMessage = (uint8_t *) decMessageIn;
	uint8_t p_dst[BUFLEN] = {0};
	sgx_status_t internalStatus=SGX_SUCCESS;
	// Generate the IV (nonce)
	sgx_read_rand(p_dst + SGX_AESGCM_MAC_SIZE, SGX_AESGCM_IV_SIZE);
	
	internalStatus=sgx_rijndael128GCM_encrypt(
		key,
		origMessage, len, 
		p_dst + SGX_AESGCM_MAC_SIZE + SGX_AESGCM_IV_SIZE,
		p_dst + SGX_AESGCM_MAC_SIZE, SGX_AESGCM_IV_SIZE,
		NULL, 0,
		(sgx_aes_gcm_128bit_tag_t *) (p_dst));
	if(internalStatus!=SGX_SUCCESS)
	{
		ocall_print("Encryption error");
	}	
		
	memcpy(encMessageOut,p_dst,lenOut);
	return internalStatus;
}

/*Functions used to initalize simulation, should be removed in the final code*/
sgx_status_t ecall_get_users(sgx_sha256_hash_t* ListHRi,uint32_t* ListRi,sgx_ec256_public_t* ListPublicKey,sgx_ec256_private_t* ListPrivateKey,sgx_ec256_signature_t* ListSignature, int nbuser) {
	sgx_status_t status=SGX_ERROR_INVALID_PARAMETER;
	sgx_ecc_state_handle_t initeccContext = NULL; 
	sgx_ec256_public_t initpublicKey;
	sgx_ec256_private_t initprivateKey;
	uint8_t resultSign;
    
	status=sgx_ecc256_open_context(&initeccContext);
	if (status!=SGX_SUCCESS)
	{
		ocall_print("fail to open ecc256 context\n");
		return status;
	}
	
	int i;
	
	for (i=0;i<nbuser;i++)
	{


		status=sgx_ecc256_create_key_pair(&ListPrivateKey[i], &ListPublicKey[i], initeccContext);
		if (status!=SGX_SUCCESS)
		{
			ocall_print("fail to create ecc256 keypair\n");
			return status;
		}


		status = sgx_read_rand((unsigned char*)&ListRi[i], 4); 

		if (status!=SGX_SUCCESS)
		{
			return status;
		}
		
		status = sgx_sha256_msg((uint8_t*)&ListRi[i], sizeof(&ListRi[i]), &ListHRi[i]);
		if (status!=SGX_SUCCESS)
		{
			return status;
		}
		status=sgx_ecdsa_sign(ListHRi[i], sizeof(sgx_sha256_hash_t), &ListPrivateKey[i], &ListSignature[i], initeccContext);
		if (status!=SGX_SUCCESS)
		{
			ocall_print("fail to perform signature");
			return status;
		}

		status=sgx_ecdsa_verify(ListHRi[i], sizeof(sgx_sha256_hash_t), &ListPublicKey[i], &ListSignature[i], &resultSign, initeccContext);
		if (status!=SGX_SUCCESS)
		{
			ocall_print("fail to verify signature");
			return status;
		}
			if(resultSign != SGX_EC_VALID)
			{
				ocall_print("signature KO");
				return SGX_ERROR_UNEXPECTED ;
			}
	}
	status=sgx_ecc256_close_context(initeccContext);

	if(status!=SGX_SUCCESS)
	{
		ocall_print("Closing context failed\n");
	}	
	return status;
}


sgx_status_t ecall_generateKeys(sgx_ec256_public_t* pubkey,sgx_ec256_private_t* privkey)
{
	sgx_ecc_state_handle_t initeccContext = NULL; 
	sgx_status_t internalStatus;
	internalStatus=sgx_ecc256_open_context(&initeccContext);
	if(internalStatus!=SGX_SUCCESS)
	{
		ocall_print("fail to open context\n");
	}

	internalStatus=sgx_ecc256_create_key_pair(privkey, pubkey, initeccContext);
	if(internalStatus!=SGX_SUCCESS)
	{
		ocall_print("fail to create keys\n");
	}


	internalStatus=sgx_ecc256_close_context(initeccContext);
	if(internalStatus!=SGX_SUCCESS)
	{
		ocall_print("fail to close context\n");
	}
	return internalStatus;

}


sgx_status_t ecall_sign(uint8_t* block, size_t blockSize,sgx_ec256_private_t privkey, sgx_ec256_signature_t* signature) {
	sgx_ecc_state_handle_t initeccContext = NULL; 
	sgx_status_t internalStatus;
	internalStatus=sgx_ecc256_open_context(&initeccContext);
	if(internalStatus!=SGX_SUCCESS)
	{
		ocall_print("fail to open context\n");
	}
	
	internalStatus=sgx_ecdsa_sign(block, blockSize, &privkey, signature, initeccContext);
	if(internalStatus!=SGX_SUCCESS)
	{
		ocall_print("fail to perform signature\n");
	}
	
	internalStatus=sgx_ecc256_close_context(initeccContext);
	if(internalStatus!=SGX_SUCCESS)
	{
		ocall_print("fail to close context\n");
	}
	return internalStatus;
}



sgx_status_t ecall_verify_sign(uint8_t* block, size_t blockSize,sgx_ec256_public_t pubkey, sgx_ec256_signature_t signature, uint8_t* result) {
	sgx_ecc_state_handle_t initeccContext = NULL; 
	sgx_status_t internalStatus;

	internalStatus=sgx_ecc256_open_context(&initeccContext);
	if(internalStatus!=SGX_SUCCESS)
	{
		ocall_print("opening context failed\n");
	}

	internalStatus=sgx_ecdsa_verify(block, blockSize, &pubkey, &signature, result, initeccContext);
	if(internalStatus!=SGX_SUCCESS)
	{
		ocall_print("Failed signature inside enclave!\n");
	}

	if(internalStatus!=SGX_SUCCESS)
	{
		ocall_print("Closing context failed");
	}	

	return internalStatus;
}

void attestation_la(sgx_key_128bit_t* dh_aek)
{
	sgx_dh_msg1_t dh_msg1;            //Diffie-Hellman Message 1
    sgx_dh_msg2_t dh_msg2;            //Diffie-Hellman Message 2
    sgx_dh_msg3_t dh_msg3;            //Diffie-Hellman Message 3
    uint32_t session_id;
    uint32_t retstatus;
    sgx_status_t status = SGX_SUCCESS;
    sgx_dh_session_t dh_session;
    sgx_dh_session_enclave_identity_t responder_identity;
    sgx_dh_session_enclave_identity_t initiator_identity;
	sgx_status_t st;

    memset(dh_aek,0, sizeof(sgx_key_128bit_t));
    memset(&dh_msg1, 0, sizeof(sgx_dh_msg1_t));
    memset(&dh_msg2, 0, sizeof(sgx_dh_msg2_t));
    memset(&dh_msg3, 0, sizeof(sgx_dh_msg3_t));
    memset(&dh_session, 0, sizeof(sgx_dh_session_t));
	//Intialize the session as a session initiator step 1
	st = sgx_dh_init_session(SGX_DH_SESSION_INITIATOR, &dh_session);
	if(st != SGX_SUCCESS)
	{
		ocall_print("Init dh session failed!!");
	}
	//step 2
	st=ocall_get_msg1_la(&dh_msg1);
	if(st != SGX_SUCCESS)
	{
		ocall_print("Fail to obtain msg1!!");
	}
	//step 3 
	st = sgx_dh_initiator_proc_msg1(&dh_msg1,&dh_msg2,&dh_session);

	if(st == SGX_ERROR_INVALID_STATE)
	{
		ocall_print("Fail to process msg1!!");
	}


	//step 4
	st=ocall_get_msg3_la(&dh_msg2,&dh_msg3,&initiator_identity);
	if(st != SGX_SUCCESS)
	{
		ocall_print("Fail to obtain msg3!!");
	}

	//step 5
	st = sgx_dh_initiator_proc_msg3(&dh_msg3,&dh_session,dh_aek,&responder_identity);
	if(st != SGX_SUCCESS)
	{
		ocall_print("Fail to process msg3!!");
	}
	


}
void attestation_ra(sgx_key_128bit_t* dh_aek)
{
	sgx_dh_msg1_t dh_msg1;            //Diffie-Hellman Message 1
    sgx_dh_msg2_t dh_msg2;            //Diffie-Hellman Message 2
    sgx_dh_msg3_t dh_msg3;            //Diffie-Hellman Message 3
    uint32_t session_id;
    uint32_t retstatus;
    sgx_status_t status = SGX_SUCCESS;
    sgx_dh_session_t dh_session;
    sgx_dh_session_enclave_identity_t responder_identity;
    sgx_dh_session_enclave_identity_t initiator_identity;
	sgx_status_t st;

    memset(&dh_msg1, 0, sizeof(sgx_dh_msg1_t));
    memset(&dh_msg2, 0, sizeof(sgx_dh_msg2_t));
    memset(&dh_msg3, 0, sizeof(sgx_dh_msg3_t));
    memset(&dh_session, 0, sizeof(sgx_dh_session_t));
	//Intialize the session as a session initiator step 1
	st = sgx_dh_init_session(SGX_DH_SESSION_INITIATOR, &dh_session);
	if(st != SGX_SUCCESS)
	{
		ocall_print("Init dh session failed!!");
	}
	//step 2
	st=ocall_get_msg1_ra(&dh_msg1);
	if(st != SGX_SUCCESS)
	{
		ocall_print("Fail to obtain msg1!!");
	}
	//step 3 
	st = sgx_dh_initiator_proc_msg1(&dh_msg1,&dh_msg2,&dh_session);

	if(st == SGX_ERROR_INVALID_STATE)
	{
		ocall_print("Fail to process msg1!!");
	}


	//step 4
	st=ocall_get_msg3_ra(&dh_msg2,&dh_msg3,&initiator_identity);
	if(st != SGX_SUCCESS)
	{
		ocall_print("Fail to obtain msg3!!");
	}

	//step 5
	st = sgx_dh_initiator_proc_msg3(&dh_msg3,&dh_session,dh_aek_succ,&responder_identity);
	if(st != SGX_SUCCESS)
	{
		ocall_print("Fail to process msg3!!");
	}


}


sgx_status_t ecall_tee_monitor(const char * manifest, sgx_ec256_signature_t signature, sgx_ec256_public_t pubkey,int* state )
{
	
	int cnt;
	sgx_status_t st;
	string man(manifest);
	vector<uint8_t> myVector(man.begin(), man.end()+1);
	uint8_t *uintManifest = &myVector[0];
	uint8_t  result=SGX_EC_INVALID_SIGNATURE;
	/* Id generation */
	
	st = sgx_ecc256_open_context(&eccContext);
	st = sgx_ecc256_create_key_pair(&privateKey, &publicKey, eccContext);
	
	/*****************/
	/*  line 1 to 2 */
	st=ecall_verify_sign(uintManifest,man.length()*sizeof(uint8_t),pubkey,signature,&result);
	if(result != SGX_EC_VALID)
	{
		ocall_print("signature KO");
		return SGX_ERROR_UNEXPECTED ;
	}
	// ocall_print(manifest);
	/* Parse Manifest */
    string delimiter = "<=>";
    size_t pos = 0;
    int M;
    int R;
	int N;
    //exctract N
	pos = man.find(delimiter);
    N = atoi((man.substr(0, pos)).c_str());

    //extract M
    man.erase(0, pos + delimiter.length());
	pos = man.find(delimiter);
    M = atoi((man.substr(0, pos)).c_str());

	//extract R
	man.erase(0, pos + delimiter.length());
	pos = man.find(delimiter);
    R = atoi((man.substr(0, pos)).c_str());
	
	string ListCat="";
	//extarct DEP
	for (cnt=0;cnt<R;cnt++)
	{
		man.erase(0, pos + delimiter.length());
		pos = man.find(delimiter);
		ListCat+=man.substr(0, pos)+"<=>";
	}
	if(state[0])
	{
		/* Built physical manifest */
		st = sgx_read_rand((unsigned char*)&random, 4); 	
		st = sgx_sha256_msg((uint8_t*)&random, sizeof(random), &hashRandom);
		st = sgx_ecdsa_sign(hashRandom, sizeof(sgx_sha256_hash_t), &privateKey, &mySignature, eccContext);
		// st = sgx_ecdsa_verify(hashRandom, sizeof(sgx_sha256_hash_t), &publicKey, &mySignature, &result, eccContext);
		if(result != SGX_EC_VALID)
		{
			ocall_print("signature personal hash KO");
		}
		// Send HRi
		st=ocall_sendHRi(&hashRandom,&publicKey,&mySignature);
		if (st!=SGX_SUCCESS)
		{
			ocall_print("Fail to send identity\n");
		}
		//Get ListHri
		hashList = (sgx_sha256_hash_t*)malloc(N*sizeof(sgx_sha256_hash_t));
		randomList = (uint32_t*)malloc(N*sizeof(uint32_t));
		randomList = (uint32_t*)malloc(N*sizeof(uint32_t));
		signatureList = (sgx_ec256_signature_t*)malloc(N*sizeof(sgx_ec256_signature_t));
		pubKeyList = (sgx_ec256_public_t*)malloc(N*sizeof(sgx_ec256_public_t));
		int j;
		st=ocall_getListHRi(hashList,pubKeyList,signatureList,&ChoosenpubKey,N);
		if (st!=SGX_SUCCESS)
		{
			ocall_print("Fail to retrieve identity list\n");
			return st;
		}
		
		//Send Ri

		st = sgx_ecdsa_sign_uint32(random, sizeof(uint32_t), &privateKey, &mySignature, eccContext);
		st = sgx_ecdsa_verify_uint32(random, sizeof(uint32_t), &publicKey, &mySignature, &result, eccContext);
		
		if(result != SGX_EC_VALID)
		{
			ocall_print("signature personal hash KO");
		}
		st=ocall_sendRi(&random,&publicKey,&mySignature);
		if (st!=SGX_SUCCESS)
		{
			ocall_print("Fail to send Ri\n");
		}


		// Get ListRi
		st=ocall_getListRi(randomList,N);
		if (st!=SGX_SUCCESS)
		{
			ocall_print("Fail to retrieve identity list\n");
			return st;
		}

		// Generate ranking random
		if(*ChoosenpubKey.gx==*publicKey.gx&&*ChoosenpubKey.gy==*publicKey.gy)
		{
			uint32_t genRand;
			st = sgx_read_rand((unsigned char*)&genRand, 4);
			st = sgx_ecdsa_sign_uint32(genRand, sizeof(uint32_t), &privateKey, &mySignature, eccContext);
			
			ocall_send_Xrand(&genRand,&mySignature);		
		}


		// get ranking random
		st=ocall_get_Xrand(&Xrandom,&XrandomSign);
		if(st != SGX_SUCCESS)
		{
			ocall_print("Getting rankin random failed !!");
		}


		// verify ranking random
		st = sgx_ecdsa_verify_uint32(Xrandom, sizeof(uint32_t), &ChoosenpubKey, &XrandomSign, &result, eccContext);
		if(result != SGX_EC_VALID)
		{
			ocall_print("Ranking random verification failed !!");
		}
		
	}
		// simulating get position
		char tmp[100];
		string reducerstask="";
		for (cnt=0;cnt<R;cnt++)
		{
			snprintf(tmp,10,"%d<=>",cnt);
			reducerstask+=tmp;
		}
	/*Launch op enclave*/
	st = ocall_launch();
	if(st != SGX_SUCCESS)
	{
		ocall_print("Launching op enclave failed!!");
	}

	if(state[1])
	{
		/*
			Remote attestation with parent 

		*/
		dh_aek_succ=(sgx_key_128bit_t*)malloc(R*sizeof(sgx_key_128bit_t));
		for(int i=0;i<R;i++)
		{
			attestation_ra(&dh_aek_succ[i]);
		}

		/* 
			send tuple to parent 
		
		*/
	}
	
	if(state[2])
	{
		/* 
			Receive children's output
		*/


		/*	local attestation	*/
		
		attestation_la(&dh_aek);


		/*Query pdms*/
		//simulated !!
		pos = ListCat.find(delimiter);
		string MyCat=ListCat.substr(0, pos);

		/*
			send tuple op enclave
			receive output from op enclave
			verify the result
		*/

		
		string messagetop=MyCat+"<=>1<=>";
		snprintf(tmp,10,"%d<=>",R);
		messagetop+=tmp+ListCat+reducerstask;

		
		size_t encMessageLen = (SGX_AESGCM_MAC_SIZE + SGX_AESGCM_IV_SIZE + strlen((char*)messagetop.c_str())+1); 
		char *encMessage = (char *) malloc((encMessageLen+1)*sizeof(char));
		
		st=encryptMessage(&dh_aek,(char*)messagetop.c_str(), strlen(messagetop.c_str()), encMessage, encMessageLen);
		if(st!=SGX_SUCCESS)
		{
			ocall_print("encryption failed!!!");
		}

		size_t decMessageLen = strlen((char*)messagetop.c_str());
		char* output=(char*)malloc(sizeof(char)*decMessageLen);

		//send encryptrd tuple to op enclave
		ocall_send_tuple(encMessage,output,encMessageLen,decMessageLen);
		
		//Decrypt result
		char *decMessage = (char *) malloc((decMessageLen+1)*sizeof(char));
		st=decryptMessage(&dh_aek,encMessage,encMessageLen,decMessage,decMessageLen);
		if(st!=SGX_SUCCESS)
		{
			ocall_print("Decryption failed!!!");
		}

		//encrypt result with parents key and send it
		for(int i=0;i<R;i++)
		{	size_t encMessageLen = (SGX_AESGCM_MAC_SIZE + SGX_AESGCM_IV_SIZE + strlen((char*)messagetop.c_str())+1); 
			char *encMessage = (char *) malloc((encMessageLen+1)*sizeof(char));
			
			st=encryptMessage(&dh_aek_succ[i],(char*)messagetop.c_str(), strlen(messagetop.c_str()), encMessage, encMessageLen);
			if(st!=SGX_SUCCESS)
			{
				ocall_print("encryption failed!!!");
			}
		
		ocall_send_result(encMessage);
		}
	}
	return st;
}











