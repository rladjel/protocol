#include "op_t.h"
#include "sgx_trts.h"
#include <vector> 
#include <stdio.h> 
#include "sgx_dh.h"
#include "string"
using namespace std;
sgx_dh_session_t dh_session;
sgx_key_128bit_t aek;
sgx_dh_session_enclave_identity_t initiator_identity;


#define BUFLEN 4096
sgx_status_t decryptMessage(sgx_key_128bit_t* key,char *encyptedMessage, size_t len, char *decMessageOut, size_t lenOut)
{
	uint8_t *encMessage = (uint8_t *) encyptedMessage;
	uint8_t p_dst[BUFLEN] = {0};
	sgx_status_t internalStatus=SGX_SUCCESS;
	internalStatus=sgx_rijndael128GCM_decrypt(
		key,
		encMessage + SGX_AESGCM_MAC_SIZE + SGX_AESGCM_IV_SIZE,
		lenOut,
		p_dst,
		encMessage + SGX_AESGCM_MAC_SIZE, 
		SGX_AESGCM_IV_SIZE,
		NULL, 0,
		(sgx_aes_gcm_128bit_tag_t *) encMessage);
	memcpy(decMessageOut, p_dst, lenOut);
	return internalStatus;
}

sgx_status_t encryptMessage(sgx_key_128bit_t* key,char *decMessageIn, size_t len, char *encMessageOut, size_t lenOut)
{
	uint8_t *origMessage = (uint8_t *) decMessageIn;
	uint8_t p_dst[BUFLEN] = {0};
	sgx_status_t internalStatus=SGX_SUCCESS;
	// Generate the IV (nonce)
	sgx_read_rand(p_dst + SGX_AESGCM_MAC_SIZE, SGX_AESGCM_IV_SIZE);
	
	internalStatus=sgx_rijndael128GCM_encrypt(
		key,
		origMessage, len, 
		p_dst + SGX_AESGCM_MAC_SIZE + SGX_AESGCM_IV_SIZE,
		p_dst + SGX_AESGCM_MAC_SIZE, SGX_AESGCM_IV_SIZE,
		NULL, 0,
		(sgx_aes_gcm_128bit_tag_t *) (p_dst));
	if(internalStatus!=SGX_SUCCESS)
	{
		ocall_print("Encryption error");
	}	
		
	memcpy(encMessageOut,p_dst,lenOut);
	return internalStatus;
}




sgx_status_t op_mapper(char* input,char* output,size_t EncryptLength,size_t DecryptLength) {
    
    sgx_status_t st;
	char *decMessage = (char *) malloc((DecryptLength+1)*sizeof(char));
	st=decryptMessage(&aek,input,EncryptLength,decMessage,DecryptLength);
    if(st!=SGX_SUCCESS)
    {
        ocall_print("op decryption failed");
    }


    string tuple(decMessage);
    string delimiter = "<=>";
    size_t pos = 0;
    int group;
    string val;
    int R;

    //exctract group
	pos = tuple.find(delimiter);
    group = atoi((tuple.substr(0, pos)).c_str());

    //extract val
    tuple.erase(0, pos + delimiter.length());
	pos = tuple.find(delimiter);
    val = tuple.substr(0, pos);



    //extract nb reducers
    tuple.erase(0, pos + delimiter.length());
	pos = tuple.find(delimiter);
    R = atoi((tuple.substr(0, pos)).c_str());
    int* ListCat=(int*)malloc(sizeof(int)*R);
    int* ListRed=(int*)malloc(sizeof(int)*R);

    int i;
    for (i=0;i<R;i++)
    {
        tuple.erase(0, pos + delimiter.length());
	    pos = tuple.find(delimiter);
        ListCat[i]=atoi((tuple.substr(0, pos)).c_str());
    }
    
    for (i=0;i<R;i++)
    {
        tuple.erase(0, pos + delimiter.length());
	    pos = tuple.find(delimiter);
        ListRed[i]=atoi((tuple.substr(0, pos)).c_str());
    }

    for (i=0;i<R;i++)
    {

        if(group==ListCat[i])
        {

            snprintf(output,20,"%d<=>%s",group,val.c_str());
        }
    }


    return SGX_SUCCESS;
}
sgx_status_t op_reducer(char* input,char* output,size_t EncryptLength,size_t DecryptLength) {
    
    sgx_status_t st;
	char *decMessage = (char *) malloc((DecryptLength+1)*sizeof(char));
	st=decryptMessage(&aek,input,EncryptLength,decMessage,DecryptLength);
    if(st!=SGX_SUCCESS)
    {
        ocall_print("op decryption failed");
    }


    string tuple(decMessage);
    string delimiter = "<=>";
    size_t pos = 0;
    int group;
    string val;
    int R;

    //exctract group
	pos = tuple.find(delimiter);
    group = atoi((tuple.substr(0, pos)).c_str());

    //extract val
    tuple.erase(0, pos + delimiter.length());
	pos = tuple.find(delimiter);
    val = tuple.substr(0, pos);



    //extract nb reducers
    tuple.erase(0, pos + delimiter.length());
	pos = tuple.find(delimiter);
    R = atoi((tuple.substr(0, pos)).c_str());
    int* ListCat=(int*)malloc(sizeof(int)*R);
    int* ListRed=(int*)malloc(sizeof(int)*R);

    int i;
    for (i=0;i<R;i++)
    {
        tuple.erase(0, pos + delimiter.length());
	    pos = tuple.find(delimiter);
        ListCat[i]=atoi((tuple.substr(0, pos)).c_str());
    }
    
    for (i=0;i<R;i++)
    {
        tuple.erase(0, pos + delimiter.length());
	    pos = tuple.find(delimiter);
        ListRed[i]=atoi((tuple.substr(0, pos)).c_str());
    }

    for (i=0;i<R;i++)
    {

        if(group==ListCat[i])
        {

            snprintf(output,20,"%d<=>%s",group,val.c_str());
        }
    }


    return SGX_SUCCESS;
}



sgx_status_t op_get_msg1(sgx_dh_msg1_t * msg1)
{
    //step 2
    sgx_status_t result;
    result = sgx_dh_init_session(SGX_DH_SESSION_RESPONDER, &dh_session);
    if (result!=SGX_SUCCESS)
    {
        ocall_print("Error init dh session op enclave");
    }
    
    
    result =sgx_dh_responder_gen_msg1(msg1,&dh_session);
    if (result!=SGX_SUCCESS)
    {
        ocall_print("Error generating msg1");
        
    }
    
    return result;

}  
sgx_status_t op_proc_msg2(sgx_dh_msg2_t * msg2, sgx_dh_msg3_t * msg3,sgx_dh_session_enclave_identity_t * initiator_identity)
{
    //step 4
    sgx_status_t status=sgx_dh_responder_proc_msg2(msg2,msg3,&dh_session,&aek,initiator_identity);
    if(status!=SGX_SUCCESS)
    {
        ocall_print("fail to process msg2 or to generate msg3");
    }
    return status;
}