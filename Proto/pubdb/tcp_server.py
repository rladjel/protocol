#!/usr/bin/python3

import threading
import socketserver
import messages
import struct

class ThreadedTCPRequestHandler(socketserver.BaseRequestHandler):
  def handle(self):
    try:
      while True:
        data_len_rx = int.from_bytes(self.request.recv(4), byteorder = 'little')
        data = str(self.request.recv(1020), 'ascii')
        response = messages.handle_data(data, data_len_rx)
        data_len_tx = len(response)
        self.request.sendall(data_len_tx.to_bytes(4, byteorder = 'little') + response)
    except Exception as e:
      # print(e)
      pass
    finally:
      print("Client disconnected")

class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
  pass
