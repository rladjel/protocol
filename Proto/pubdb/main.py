#!/usr/bin/python3

import sys
import threading
from tcp_server import *

def main(args):
  server = ThreadedTCPServer(("localhost", 1234), ThreadedTCPRequestHandler)
  ip, port = server.server_address
  server_thread = threading.Thread(target = server.serve_forever)
  server_thread.start()

  print("PubDB server is running at {}:{}".format(ip, port))

if __name__ == '__main__':
  main(sys.argv[1:])
