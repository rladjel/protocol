#!/usr/bin/python3

import tools
import base64
import json
import struct
from pathlib import Path

JSON_CLIENTID_STRING = "client_id"
JSON_MESSAGE_STRING = "message"
JSON_VALUE_STRING = "value"

VALUE_OK = "OK"

GET_MANIFEST_MESSAGE_STRING = "get_manifest"
ADD_HASH_MESSAGE_STRING = "add_hash"
GET_HASH_LIST_MESSAGE_STRING = "get_hash_list"
ADD_RANDOM_MESSAGE_STRING = "add_random"
GET_RANDOM_LIST_MESSAGE_STRING = "get_random_list"
ADD_RANK_MESSAGE_STRING = "add_rank"
GET_RANK_MESSAGE_STRING = "get_rank"
UNKNOWN_MESSAGE_STRING = "unknown_message"
SIM_CLIENT_SEND_MESSAGE_STRING = "sim_client_send"
SIM_CLIENT_RECEIVE_MESSAGE_STRING = "sim_client_receive"

RANK_SELECTED = "2"
CLIENT_MESSAGE = "this is supposed to be the data that one client is supposed to sent the other"

hash_list = list()
random_list = list()
rank_list = list()

def add_hash(client_id, value):
  global hash_list
  if value not in hash_list:
    hash_list.append(value)
  # hash_list.append([int(client_id), value])
  # print(hash_list)
  # print("hash:{} received from {}".format(value, client_id))
  return tools.from_json_to_string({JSON_MESSAGE_STRING: ADD_HASH_MESSAGE_STRING, JSON_VALUE_STRING: VALUE_OK})

def sim_client_send(client_id, value):
  return tools.from_json_to_string({JSON_MESSAGE_STRING: SIM_CLIENT_SEND_MESSAGE_STRING, JSON_VALUE_STRING: VALUE_OK})

def sim_client_receive():
  value = CLIENT_MESSAGE
  return tools.from_json_to_string({JSON_MESSAGE_STRING: GET_RANDOM_LIST_MESSAGE_STRING, JSON_VALUE_STRING: value})

def get_hash_list():
  global hash_list
  value = json.dumps(hash_list)
  return tools.from_json_to_string({JSON_MESSAGE_STRING: GET_HASH_LIST_MESSAGE_STRING, JSON_VALUE_STRING: value})

def add_random(client_id, value):
  global random_list
  if value not in random_list:
    random_list.append(value)
  # random_list.append([int(client_id), value])
  # print(random_list)
  return tools.from_json_to_string({JSON_MESSAGE_STRING: ADD_RANDOM_MESSAGE_STRING, JSON_VALUE_STRING: VALUE_OK})

def get_random_list():
  global random_list
  value = json.dumps(random_list)
  return tools.from_json_to_string({JSON_MESSAGE_STRING: GET_RANDOM_LIST_MESSAGE_STRING, JSON_VALUE_STRING: value})

def add_rank(client_id, value):
  global rank_list
  if value not in rank_list:
    rank_list.append(value)
  # rank_list.append([int(client_id), value])
  # print(rank_list)
  return tools.from_json_to_string({JSON_MESSAGE_STRING: ADD_RANK_MESSAGE_STRING, JSON_VALUE_STRING: VALUE_OK})

def get_rank():
  # global rank_list
  # value = json.dumps(rank_list)
  value = RANK_SELECTED
  return tools.from_json_to_string({JSON_MESSAGE_STRING: GET_RANK_MESSAGE_STRING, JSON_VALUE_STRING: value})

def get_manifest():
  with open('utility/manifest.bin', mode="rb") as f:
    c = f.read()
  value_bytes = base64.b64encode(c)
  value = value_bytes.decode("utf-8")
  return tools.from_json_to_string({JSON_MESSAGE_STRING: GET_MANIFEST_MESSAGE_STRING, JSON_VALUE_STRING: value})

def unknown_message():
  return tools.from_json_to_string({JSON_MESSAGE_STRING: UNKNOWN_MESSAGE_STRING})

def handle_data(data, data_len):
  jsonobj = tools.from_string_to_json(data)
  client_id = jsonobj[JSON_CLIENTID_STRING]
  msg_type = jsonobj[JSON_MESSAGE_STRING]
  try:
    value = jsonobj[JSON_VALUE_STRING]
  except:
    value = None
  print("{} message received from client ID {}".format(msg_type, client_id))

  if msg_type == GET_MANIFEST_MESSAGE_STRING:
    response = get_manifest()
  elif msg_type == ADD_HASH_MESSAGE_STRING:
    response = add_hash(client_id, value)
  elif msg_type == GET_HASH_LIST_MESSAGE_STRING:
    response = get_hash_list()
  elif msg_type == ADD_RANDOM_MESSAGE_STRING:
    response = add_random(client_id, value)
  elif msg_type == GET_RANDOM_LIST_MESSAGE_STRING:
    response = get_random_list()
  elif msg_type == ADD_RANK_MESSAGE_STRING:
    response = add_rank(client_id, value)
  elif msg_type == GET_RANK_MESSAGE_STRING:
    response = get_rank()
  elif msg_type == SIM_CLIENT_SEND_MESSAGE_STRING:
    response = sim_client_send(client_id, value)
  elif msg_type == SIM_CLIENT_RECEIVE_MESSAGE_STRING:
    response = sim_client_receive()
  else:
    response = unknown_message()
  
  return bytes(response, 'ascii')
