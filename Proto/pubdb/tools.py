#!/usr/bin/python3

import json

def from_json_to_string(json_obj):
  return json.dumps(json_obj)

def from_string_to_json(json_str):
  return json.loads(json_str)
