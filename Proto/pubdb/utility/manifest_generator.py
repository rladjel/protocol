#!/usr/bin/python3

import sys
from pathlib import Path

def main(args):
  manifest_string = "some bullshit string"

  c1 = manifest_string.encode('utf-8')
  c1_len = len(c1).to_bytes(4, byteorder="little")

  with open('input/app', mode="rb") as f:
    c2 = f.read()
  c2_len = len(c2).to_bytes(4, byteorder="little")

  with open('input/Monitor.signed.so', mode="rb") as f:
    c3 = f.read()
  c3_len = len(c3).to_bytes(4, byteorder="little")

  with open('input/op.signed.so', mode="rb") as f:
    c4 = f.read()
  c4_len = len(c4).to_bytes(4, byteorder="little")

  # merge content (sub content length + sub content)
  c = c1_len + c1 + c2_len + c2 + c3_len + c3 + c4_len + c4

  with open("manifest.bin","wb") as f:
    f.write(c)

if __name__ == '__main__':
    main(sys.argv[1:])
