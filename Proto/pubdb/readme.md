# server start
python3 main.py

# client connection under linux
netcat localhost 1234

# messages
## getting the manifest
request: *{"client_id": "1", "message": "get_manifest"}* <br/>
answer: *{"message": "get_manifest", "value": "base64_encoded_manifest_file"}*
## adding a hash to the list
request: *{"client_id": "1", "message": "add_hash", "value": "hash_string_1"}* <br/>
answer: *{"message": "add_hash", "value": "OK"}*
## getting hash list
request: *{"client_id": "1", "message": "get_hash_list"}* <br/>
answer: *{"message": "get_hash_list", "value": "hash_list"}*
## adding a random to the list
request: *{"client_id": "1", "message": "add_random", "value": "random_no_string_1"}* <br/>
answer: *{"message": "add_random", "value": "OK"}*
## getting random list
request: *{"client_id": "1", "message": "get_random_list"}* <br/>
answer: *{"message": "get_random_list", "value": "random_list"}*
## adding a rank to the list
request: *{"client_id": "1", "message": "add_rank", "value": "rank_string_1"}* <br/>
answer: *{"message": "add_rank", "value": "OK"}*
## getting the selected rank
request: *{"client_id": "1", "message": "get_rank"}* <br/>
answer: *{"message": "get_rank", "value": "selected_rank_string"}*
## simulating client message sending
request: *{"client_id": "1", "message": "sim_client_send", "value": "rx_data_in_string"}* <br/>
answer: *{"message": "sim_client_send", "value": "OK"}*
## simulating client message receiving
request: *{"client_id": "1", "message": "sim_client_receive"}* <br/>
answer: *{"message": "sim_client_send", "value": "tx_data_in_string"}*
