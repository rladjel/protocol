#ifndef __TCP_H_
#define __TCP_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

void tcp_send(const void *write_buffer, uint32_t nbytes_to_write);
uint32_t tcp_receive(void *recv_buff);
void tcp_connect(const char *ip, const char *port);

#ifdef __cplusplus
}
#endif

#endif /* __TCP_H_ */
