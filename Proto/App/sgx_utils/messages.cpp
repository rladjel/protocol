#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "jsmn.h"
#include "common.h"
#include "messages.h"
#include "tcp.h"
#include "base64.h"
using namespace std;
#define MAX_MANIFEST_SIZE (1024 * 1024 * 2)
#define SEND_BUFF_SIZE (1024)
#define RECV_BUFF_SIZE (MAX_MANIFEST_SIZE)

char recv_buff[RECV_BUFF_SIZE];
char send_buff[SEND_BUFF_SIZE];
int ret;
struct stat st = {0};
FILE *f;
uint32_t section_len;
uint32_t manifest_len;
char *cursor;
char *res;
char manifest[MAX_MANIFEST_SIZE];

static int jsoneq(const char *json, jsmntok_t *tok, const char *s);
static int handle_add_hash(void);
static int handle_add_random(void);
static int handle_get_manifest(void);
static int handle_get_hash_list(char hash_list[MAX_CLIENT_NUM][MAX_HASH_SIZE]);
static int handle_get_random_list(char random_list[MAX_CLIENT_NUM][MAX_RND_SIZE]);
static int handle_add_rank(void);
static int handle_get_rank(char *selected_rank);
static int handle_sim_client_send(void);
static int handle_sim_client_receive(char *msg);
static void init_buffers(void);
static void json_exchange(void);

int get_manifest(void)
{
  char *json_str = "{\"client_id\": \"%s\", \"message\": \"get_manifest\"}";
  init_buffers();
  sprintf(send_buff, json_str, get_client_id());
  json_exchange();
  return handle_get_manifest();
}

static int handle_get_manifest(void)
{
  int i, r;
	jsmn_parser p;
	jsmntok_t t[128]; /* We expect no more than 128 tokens */

  jsmn_init(&p);
	r = jsmn_parse(&p, recv_buff, strlen(recv_buff), t, (sizeof(t) / sizeof(t[0])));
	if (r < 0)
  {
		printf("Failed to parse JSON: %d\n", r);
		exit(EXIT_FAILURE);
	}

	/* Assume the top-level element is an object */
	if (r < 1 || t[0].type != JSMN_OBJECT)
  {
		printf("Object expected\n");
		exit(EXIT_FAILURE);
	}

  /* Loop over all keys of the root object */
	for (i = 1; i < r; i++)
  {
    if (jsoneq(recv_buff, &t[i], "value") == 0)
    {

      res = strndup(recv_buff + t[i + 1].start, t[i + 1].end - t[i + 1].start);

      if (stat("manifest", &st) == -1)
      {
        mkdir("manifest", 0700);
        printf("subfolder has been created\n");
      }
      else
      {
        printf("subfolder already exists\n");
      }

      base64_decode((const unsigned char *)res, manifest, &manifest_len, MAX_MANIFEST_SIZE);

      // saving manifest string
      f = fopen("manifest/string.txt", "w");
      if (f == NULL)
      {
        printf("Error: fopen\n");
        exit(EXIT_FAILURE);
      }
      section_len = *((uint32_t *)manifest);
      fprintf(f, "%*.*s", section_len, section_len, manifest + sizeof(uint32_t));
      fclose(f);
      cursor = manifest + section_len + sizeof(uint32_t);

      // saving app
      f = fopen("manifest/app", "w");
      if (f == NULL)
      {
        printf("Error: fopen\n");
        exit(EXIT_FAILURE);
      }
      section_len = *((uint32_t *)cursor);
      fwrite(cursor + sizeof(uint32_t), 1, section_len, f) ;
      fclose(f);
      cursor = cursor + section_len + sizeof(uint32_t);

      // saving enclave.signed.so
      f = fopen("manifest/enclave.signed.so", "w");
      if (f == NULL)
      {
        printf("Error: fopen\n");
        exit(EXIT_FAILURE);
      }
      section_len = *((uint32_t *)cursor);
      fwrite(cursor + sizeof(uint32_t), 1, section_len, f) ;
      fclose(f);
      cursor = cursor + section_len + sizeof(uint32_t);

      // saving op.signed.so
      f = fopen("manifest/op.signed.so", "w");
      if (f == NULL)
      {
        printf("Error: fopen\n");
        exit(EXIT_FAILURE);
      }
      section_len = *((uint32_t *)cursor);
      fwrite(cursor + sizeof(uint32_t), 1, section_len, f) ;
      fclose(f);
      cursor = cursor + section_len + sizeof(uint32_t);

      return SUCCESS;
		}
  }

  return ERROR;
}

int sim_client_send(char *msg)
{
  char *json_str = "{\"client_id\": \"%s\", \"message\": \"sim_client_send\", \"value\": \"%s\"}";
  init_buffers();
  sprintf(send_buff, json_str, get_client_id(), msg);
  json_exchange();
  return handle_sim_client_send();
}

int add_hash(char *hash)
{
  char *json_str = "{\"client_id\": \"%s\", \"message\": \"add_hash\", \"value\": \"%s\"}";
  init_buffers();
  sprintf(send_buff, json_str, get_client_id(), hash);
  json_exchange();
  return handle_add_hash();
}

int add_random(char *random)
{
  char *json_str = "{\"client_id\": \"%s\", \"message\": \"add_random\", \"value\": \"%s\"}";
  init_buffers();
  sprintf(send_buff, json_str, get_client_id(), random);
  json_exchange();
  return handle_add_random();
}

int add_rank(char rank)
{
  char *json_str = "{\"client_id\": \"%s\", \"message\": \"add_rank\", \"value\": \"%d\"}";
  init_buffers();
  sprintf(send_buff, json_str, get_client_id(), rank);
  json_exchange();
  return handle_add_rank();
}

static int handle_add_hash(void)
{
  int i, r;
	jsmn_parser p;
	jsmntok_t t[128]; /* We expect no more than 128 tokens */

  jsmn_init(&p);
	r = jsmn_parse(&p, recv_buff, strlen(recv_buff), t, (sizeof(t) / sizeof(t[0])));
	if (r < 0)
  {
		printf("Failed to parse JSON: %d\n", r);
		exit(EXIT_FAILURE);
	}

	/* Assume the top-level element is an object */
	if (r < 1 || t[0].type != JSMN_OBJECT)
  {
		printf("Object expected\n");
		exit(EXIT_FAILURE);
	}

  /* Loop over all keys of the root object */
	for (i = 1; i < r; i++)
  {
    if (jsoneq(recv_buff, &t[i], "value") == 0)
    {
      char *res = strndup(recv_buff + t[i + 1].start, t[i + 1].end - t[i + 1].start);

      if (strcmp("OK", res) == 0)
        return SUCCESS;
		}
  }

  return ERROR;
}

static int handle_sim_client_send(void)
{
  int i, r;
	jsmn_parser p;
	jsmntok_t t[128]; /* We expect no more than 128 tokens */

  jsmn_init(&p);
	r = jsmn_parse(&p, recv_buff, strlen(recv_buff), t, (sizeof(t) / sizeof(t[0])));
	if (r < 0)
  {
		printf("Failed to parse JSON: %d\n", r);
		exit(EXIT_FAILURE);
	}

	/* Assume the top-level element is an object */
	if (r < 1 || t[0].type != JSMN_OBJECT)
  {
		printf("Object expected\n");
		exit(EXIT_FAILURE);
	}

  /* Loop over all keys of the root object */
	for (i = 1; i < r; i++)
  {
    if (jsoneq(recv_buff, &t[i], "value") == 0)
    {
      char *res = strndup(recv_buff + t[i + 1].start, t[i + 1].end - t[i + 1].start);

      if (strcmp("OK", res) == 0)
        return SUCCESS;
		}
  }

  return ERROR;
}

static int handle_add_random(void)
{
  int i, r;
	jsmn_parser p;
	jsmntok_t t[128]; /* We expect no more than 128 tokens */

  jsmn_init(&p);
	r = jsmn_parse(&p, recv_buff, strlen(recv_buff), t, (sizeof(t) / sizeof(t[0])));
	if (r < 0)
  {
		printf("Failed to parse JSON: %d\n", r);
		exit(EXIT_FAILURE);
	}

	/* Assume the top-level element is an object */
	if (r < 1 || t[0].type != JSMN_OBJECT)
  {
		printf("Object expected\n");
		exit(EXIT_FAILURE);
	}

  /* Loop over all keys of the root object */
	for (i = 1; i < r; i++)
  {
    if (jsoneq(recv_buff, &t[i], "value") == 0)
    {
      char *res = strndup(recv_buff + t[i + 1].start, t[i + 1].end - t[i + 1].start);

      if (strcmp("OK", res) == 0)
        return SUCCESS;
		}
  }

  return ERROR;
}

static int handle_add_rank(void)
{
  int i, r;
	jsmn_parser p;
	jsmntok_t t[128]; /* We expect no more than 128 tokens */

  jsmn_init(&p);
	r = jsmn_parse(&p, recv_buff, strlen(recv_buff), t, (sizeof(t) / sizeof(t[0])));
	if (r < 0)
  {
		printf("Failed to parse JSON: %d\n", r);
		exit(EXIT_FAILURE);
	}

	/* Assume the top-level element is an object */
	if (r < 1 || t[0].type != JSMN_OBJECT)
  {
		printf("Object expected\n");
		exit(EXIT_FAILURE);
	}

  /* Loop over all keys of the root object */
	for (i = 1; i < r; i++)
  {
    if (jsoneq(recv_buff, &t[i], "value") == 0)
    {
      char *res = strndup(recv_buff + t[i + 1].start, t[i + 1].end - t[i + 1].start);

      if (strcmp("OK", res) == 0)
        return SUCCESS;
		}
  }

  return ERROR;
}

int get_hash_list(char hash_list[MAX_CLIENT_NUM][MAX_HASH_SIZE])
{
  char *get_hash_list_req_str = "{\"client_id\": \"%s\", \"message\": \"get_hash_list\"}";
  init_buffers();
  sprintf(send_buff, get_hash_list_req_str, get_client_id());
  json_exchange();
  return handle_get_hash_list(hash_list);
}

int sim_client_receive(char *msg)
{
  char *json_str = "{\"client_id\": \"%s\", \"message\": \"sim_client_receive\"}";
  init_buffers();
  sprintf(send_buff, json_str, get_client_id());
  json_exchange();
  return handle_sim_client_receive(msg);
}

int get_random_list(char random_list[MAX_CLIENT_NUM][MAX_RND_SIZE])
{
  char *get_random_list_req_str = "{\"client_id\": \"%s\", \"message\": \"get_random_list\"}";
  init_buffers();
  sprintf(send_buff, get_random_list_req_str, get_client_id());
  json_exchange();
  return handle_get_random_list(random_list);
}

int get_rank(char *selected_rank)
{
  char *get_rank_req_str = "{\"client_id\": \"%s\", \"message\": \"get_rank\"}";
  init_buffers();
  sprintf(send_buff, get_rank_req_str, get_client_id());
  json_exchange();
  return handle_get_rank(selected_rank);
}

static int handle_get_hash_list(char hash_list[MAX_CLIENT_NUM][MAX_HASH_SIZE])
{
  int i;
	int r;
	jsmn_parser p;
  uint8_t client_cnt = 0;
	jsmntok_t t[128]; /* We expect no more than 128 tokens */

  jsmn_init(&p);
	r = jsmn_parse(&p, recv_buff, strlen(recv_buff), t, (sizeof(t) / sizeof(t[0])));
	if (r < 0)
  {
		printf("Failed to parse JSON: %d\n", r);
		exit(EXIT_FAILURE);
	}

	/* Assume the top-level element is an object */
	if (r < 1 || t[0].type != JSMN_OBJECT)
  {
		printf("Object expected\n");
		exit(EXIT_FAILURE);
	}

  /* Loop over all keys of the root object */
	for (i = 1; i < r; i++)
  {
    if (jsoneq(recv_buff, &t[i], "value") == 0)
    {
      char temp_buff[MAX_HASH_SIZE * MAX_CLIENT_NUM];
      memcpy(temp_buff, recv_buff + t[i+1].start, t[i+1].end - t[i+1].start);
      char *p = strtok(temp_buff, "\\");
      while(p != NULL)
      {
        // Save the value in pointer p
        p = strtok(NULL, "\\");
        if (p != NULL)
        {
          if (strlen(p) >= MAX_HASH_SIZE)
          {
            memcpy(&hash_list[client_cnt++], p+1, MAX_HASH_SIZE);
            if (client_cnt >= MAX_CLIENT_NUM)
            {
              // possibly some clients sent their hash more than once
              printf("Error: hash list too big\n");
		          exit(EXIT_FAILURE);
            }
          }
        }
      }
			i++;
		}
  }

  return client_cnt;
}

static int handle_sim_client_receive(char *msg)
{
  int i;
	int r;
	jsmn_parser p;
  uint8_t client_cnt = 0;
	jsmntok_t t[128]; /* We expect no more than 128 tokens */

  jsmn_init(&p);
	r = jsmn_parse(&p, recv_buff, strlen(recv_buff), t, (sizeof(t) / sizeof(t[0])));
	if (r < 0)
  {
		printf("Failed to parse JSON: %d\n", r);
		exit(EXIT_FAILURE);
	}

	/* Assume the top-level element is an object */
	if (r < 1 || t[0].type != JSMN_OBJECT)
  {
		printf("Object expected\n");
		exit(EXIT_FAILURE);
	}

  /* Loop over all keys of the root object */
	for (i = 1; i < r; i++)
  {
    if (jsoneq(recv_buff, &t[i], "value") == 0)
      memcpy(msg, recv_buff + t[i+1].start, t[i+1].end - t[i+1].start);
  }

  return client_cnt;
}

static int handle_get_random_list(char random_list[MAX_CLIENT_NUM][MAX_RND_SIZE])
{
  int i;
	int r;
	jsmn_parser p;
  uint8_t client_cnt = 0;
	jsmntok_t t[128]; /* We expect no more than 128 tokens */

  jsmn_init(&p);
	r = jsmn_parse(&p, recv_buff, strlen(recv_buff), t, (sizeof(t) / sizeof(t[0])));
	if (r < 0)
  {
		printf("Failed to parse JSON: %d\n", r);
		exit(EXIT_FAILURE);
	}

	/* Assume the top-level element is an object */
	if (r < 1 || t[0].type != JSMN_OBJECT)
  {
		printf("Object expected\n");
		exit(EXIT_FAILURE);
	}

  /* Loop over all keys of the root object */
	for (i = 1; i < r; i++)
  {
    if (jsoneq(recv_buff, &t[i], "value") == 0)
    {
      char temp_buff[MAX_RND_SIZE * MAX_CLIENT_NUM];
      memcpy(temp_buff, recv_buff + t[i+1].start, t[i+1].end - t[i+1].start);
      char *p = strtok(temp_buff, "\\");
      while(p != NULL)
      {
        // Save the value in pointer p
        p = strtok(NULL, "\\");
        if (p != NULL)
        {
          if (strlen(p) >= MAX_RND_SIZE)
          {
            memcpy(&random_list[client_cnt++], p+1, MAX_RND_SIZE);
            if (client_cnt >= MAX_CLIENT_NUM)
            {
              // possibly some clients sent their random numbers more than once
              printf("Error: random list too big\n");
		          exit(EXIT_FAILURE);
            }
          }
        }
      }
			i++;
		}
  }

  return client_cnt;
}

static int handle_get_rank(char *selected_rank)
{
  int i;
	int r;
	jsmn_parser p;
  uint8_t client_cnt = 0;
	jsmntok_t t[128]; /* We expect no more than 128 tokens */

  jsmn_init(&p);
	r = jsmn_parse(&p, recv_buff, strlen(recv_buff), t, (sizeof(t) / sizeof(t[0])));
	if (r < 0)
  {
		printf("Failed to parse JSON: %d\n", r);
		exit(EXIT_FAILURE);
	}

	/* Assume the top-level element is an object */
	if (r < 1 || t[0].type != JSMN_OBJECT)
  {
		printf("Object expected\n");
		exit(EXIT_FAILURE);
	}

  /* Loop over all keys of the root object */
	for (i = 1; i < r; i++)
  {
    if (jsoneq(recv_buff, &t[i], "value") == 0)
    {
      memcpy(selected_rank, recv_buff + t[i+1].start, 1);
			i++;
		}
  }

  return client_cnt;
}

void init_messages(void)
{
  init_buffers();
}

static int jsoneq(const char *json, jsmntok_t *tok, const char *s)
{
	if (tok->type == JSMN_STRING && (int) strlen(s) == tok->end - tok->start &&
			strncmp(json + tok->start, s, tok->end - tok->start) == 0)
		return 0;
	return -1;
}

static void init_buffers(void)
{
  memset(recv_buff, 0, RECV_BUFF_SIZE);
  memset(send_buff, 0, SEND_BUFF_SIZE);  
}

static void json_exchange(void)
{
  tcp_send(send_buff, strlen(send_buff));
  tcp_receive(recv_buff);
}
