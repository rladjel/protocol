#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "common.h"
#include "base64.h"
#include "apr_base64.h"

uint8_t base64_encode(const char *in, int32_t in_size, uint8_t *out, uint32_t* out_size, uint32_t out_limit)
{
	if (out_limit < apr_base64_encode_len(in_size))
	{
    printf("Error: base64_encode\n");
		exit(EXIT_FAILURE);
	}

	*out_size = (uint32_t) apr_base64_encode_binary((char*)out, (const unsigned char *)in, in_size);
	return SUCCESS;
}

uint8_t base64_decode(const uint8_t* in, char *out, uint32_t* out_size, uint32_t out_limit)
{
	if (out_limit < apr_base64_decode_len((const char*)in))
	{
		printf("Error: base64_decode\n");
		exit(EXIT_FAILURE);
	}

	*out_size = (uint32_t) apr_base64_decode_binary((unsigned char *)out, (const char*)in);
	return SUCCESS;
}


static const unsigned char table[256] =
{
	65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65,
	65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65,
	65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 62, 65, 65, 65, 63,
	52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 65, 65, 65, 64, 65, 65,
	65,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14,
	15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 65, 65, 65, 65, 65,
	65, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
	41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 65, 65, 65, 65, 65,
	65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65,
	65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65,
	65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65,
	65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65,
	65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65,
	65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65,
	65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65,
	65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65
};

//return true if in is a valid base64 encoded
bool is_base64(const char *in, int32_t in_size)
{
	int32_t computed_size;
	const unsigned char* tmp;

	tmp = (const unsigned char *) in;
	while (table[*(tmp++)] <= 64);
	computed_size = (tmp - (const unsigned char *) in) - 1;

	return computed_size == in_size;
}