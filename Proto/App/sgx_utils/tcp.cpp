#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>
#include "common.h"
#include "tcp.h"

static int sockfd = 0;

void tcp_send(const void *write_buffer, uint32_t nbytes_to_write)
{
  uint32_t nbytes_written;

  nbytes_written = send(sockfd, &nbytes_to_write, sizeof(uint32_t), 0); // send data length
  nbytes_written += send(sockfd, write_buffer, nbytes_to_write, 0); // send data
  if (nbytes_written == -1)
  {
    printf("Error: send\n");
    exit(EXIT_FAILURE);
  }
  else
  {
#ifdef TRACE_ENABLED
    printf("Sent: %s\n", (const char *)write_buffer);
#endif
  }
}

uint32_t tcp_receive(void *recv_buff)
{
  int n = 0, total_size = 0;
  uint32_t nbytes_to_receive;

  if (read(sockfd, &nbytes_to_receive, sizeof(uint32_t)) < 0)
  {
    printf("Error: read data length\n");
    exit(EXIT_FAILURE);
  }

	while(1)
	{
		if((n = read(sockfd, (recv_buff + n), nbytes_to_receive)) < 0)
		{ 
      printf("Error: read data length\n");
			exit(EXIT_FAILURE);
		}
		else
		{
      total_size += n;
      if (total_size == nbytes_to_receive)
        break;
		}
	}

#ifdef TRACE_ENABLED
  printf("Received: %s\n", (const char *)recv_buff);
#endif
  return total_size;
}

void tcp_connect(const char *ip, const char *port)
{
  struct sockaddr_in serv_addr;

  if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    printf("Error: Could not create socket\n");
    exit(EXIT_FAILURE);
  } 

  memset(&serv_addr, '0', sizeof(serv_addr)); 

  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(atoi(port)); 

  if(inet_pton(AF_INET, ip, &serv_addr.sin_addr) <= 0)
  {
    printf("Error: inet_pton\n");
    exit(EXIT_FAILURE);
  } 

  if(connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
  {
    printf("Error: Connection failed\n");
    exit(EXIT_FAILURE);
  }
}