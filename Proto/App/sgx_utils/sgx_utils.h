#ifndef _SGX_UTILS_H_
#define _SGX_UTILS_H_
#include <stdio.h>

#include "../Monitor_u.h"
#include "../op_u.h"
#include "sgx_urts.h"

#if defined(_MSC_VER)
#include <Shlobj.h>
#define TOKEN_FILENAME   "Monitor.token"
#define ENCLAVE_FILENAME "Monitor.signed.dll"
#elif defined(__GNUC__)
# include <unistd.h>
# include <pwd.h>
# define MAX_PATH FILENAME_MAX
#define TOKEN_FILENAME   "Monitor.token"
#define ENCLAVE_FILENAME "Monitor.signed.so"
#define TOKEN_FILENAME_op   "op.token"
#define ENCLAVE_FILENAME_op "op.signed.so"
#endif

#ifndef TRUE
# define TRUE 1
#endif

#ifndef FALSE
# define FALSE 0
#endif

typedef struct _sgx_errlist_t {
	sgx_status_t err;
	const char *msg;
	const char *sug; /* Suggestion */
} sgx_errlist_t;

/* Error code returned by sgx_create_enclave */

int initialize_enclave_monitor(sgx_enclave_id_t* global_eid);
int initialize_enclave_op(sgx_enclave_id_t* global_eid);
void print_error_message(sgx_status_t ret);
#endif /* !_SGX_UTILS_H_ */