#ifndef __COMMON_H_
#define __COMMON_H_

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

#define NUMBER_OF_TEST_CLIENTS (4)

#define MAX_HASH_SIZE (64)

#define SUCCESS (0)
#define ERROR (!SUCCESS)

char *get_client_id(void);
char *get_hash(void);

extern char sample_hash[MAX_HASH_SIZE];

#ifdef __cplusplus
}
#endif

#endif /* __COMMON_H_ */
