
#ifndef DM_API_BASE64_H
#define DM_API_BASE64_H

#include <stdbool.h>
#include <stdint.h>

uint8_t base64_encode(const char* in, int32_t in_size, uint8_t* out, uint32_t* out_size, uint32_t out_limit);
uint8_t base64_decode(const uint8_t* in, char* out, uint32_t* out_size, uint32_t out_limit);
bool is_base64(const char *in, int32_t in_size);

#endif	/* DM_API_BASE64_H */