#ifndef __MESSAGES_H_
#define __MESSAGES_H_

#include <stdint.h>
#include "common.h"

#ifdef __cplusplus
extern "C" {
#endif

#define MAX_CLIENT_NUM (1000)
#define MAX_RND_SIZE (16)

void init_messages(void);
int get_manifest(void);
int add_hash(char *hash);
int get_hash_list(char hash_list[MAX_CLIENT_NUM][MAX_HASH_SIZE]);
int add_random(char *random);
int get_random_list(char random_list[MAX_CLIENT_NUM][MAX_RND_SIZE]);
int add_rank(char random);
int get_rank(char *selected_rank);
int sim_client_send(char *sample_sim_client_receive);
int sim_client_receive(char *sample_sim_client_receive);

#ifdef __cplusplus
}
#endif

#endif /* __MESSAGES_H_ */
