#include "App.h"
#include "omp.h"
#include "sgx_utils/tcp.h"
#include "sgx_utils/common.h"
#include "sgx_utils/messages.h"

using namespace std;
#define LOG 1


const int MAPPERS=100;
const int REDUCERS=10;
const int TOTALUSERS=MAPPERS+REDUCERS;
const int BUILD=1;
const int ATTEST=1;
const int COMPUTE=1;
const int MAXATTEMP=1;

int state[]={BUILD,ATTEST,COMPUTE}	;

/*Global Variable used for init simul*/
sgx_sha256_hash_t* ListHRi=(sgx_sha256_hash_t*)malloc(TOTALUSERS*sizeof(sgx_sha256_hash_t));
uint32_t* ListRi=(uint32_t*)malloc(TOTALUSERS*sizeof(uint32_t));//change to uint32 
int **UsersData = (int **)malloc(TOTALUSERS* sizeof(int *)); 
sgx_ec256_signature_t* ListSignature=(sgx_ec256_signature_t*)malloc(TOTALUSERS*sizeof(sgx_ec256_signature_t));
sgx_ec256_public_t* initpublicKey=(sgx_ec256_public_t*)malloc(TOTALUSERS*sizeof(sgx_ec256_public_t));;
sgx_ec256_private_t* initprivateKey=(sgx_ec256_private_t*)malloc(TOTALUSERS*sizeof(sgx_ec256_private_t));;

/* Global EID shared by multiple threads */
sgx_enclave_id_t global_eid = 0;
sgx_enclave_id_t global_eid_op = 1;
uint32_t Xrandom;
sgx_ec256_signature_t Xsignature;
#define CLIENT_ID_SIZE (4)
char sample_hash[MAX_HASH_SIZE];
char client_id[CLIENT_ID_SIZE];
static void set_client_id(char *cid);
char *get_client_id(void);
static void set_hash(char *hash);
char *get_hash(void);
void ocall_print(const char* str);
void ocall_print_key(sgx_key_128bit_t* str);
void ocall_print_hash(sgx_sha256_hash_t* str);
void ocall_print_sign(sgx_ec256_signature_t* str);
void ocall_print_pubk(sgx_ec256_public_t* str);
void ocall_print_target(sgx_dh_msg1_t* str);
void ocall_print_privk(sgx_ec256_private_t* str);
void ocall_print_dh_session(sgx_dh_session_t* str);
void ocall_print_uint(uint32_t str);
void ocall_sendHRi(sgx_sha256_hash_t* HRi,sgx_ec256_public_t* PKi,sgx_ec256_signature_t* sign);
void ocall_sendRi(uint32_t* Ri,sgx_ec256_public_t* PKi,sgx_ec256_signature_t* sign);
void ocall_getListHRi(sgx_sha256_hash_t* LHRi,sgx_ec256_public_t* LPKi,sgx_ec256_signature_t* Lsign,sgx_ec256_public_t* ChoosenpubKey, int N);
void ocall_getListRi(uint32_t* LRi, int N);
void ocall_send_Xrand(uint32_t* Xrand,sgx_ec256_signature_t* signature);
void ocall_get_Xrand(uint32_t* Xrand,sgx_ec256_signature_t* signature);
void ocall_launch();
void ocall_send_tuple(char* message, char* output,size_t EncryptLength,size_t DecryptLength);
void ocall_get_msg1_la(sgx_dh_msg1_t * msg1);
void ocall_send_result(char* result);
void ocall_get_msg3_la(sgx_dh_msg2_t * msg2, sgx_dh_msg3_t * msg3,sgx_dh_session_enclave_identity_t * initiator_identity);
void ocall_get_msg1_ra(sgx_dh_msg1_t * msg1);
void ocall_get_msg3_ra(sgx_dh_msg2_t * msg2, sgx_dh_msg3_t * msg3,sgx_dh_session_enclave_identity_t * initiator_identity);
string converter(uint8_t *str);

int main()
{

	/************************simulation init to be removed*************************/

	/*users' information generation*/
	srand(time(NULL));
	int i;
	cout<<"Users' information generation"<<endl;
	if (initialize_enclave_monitor(&global_eid) < 0) {
		fprintf(stderr, "Fail to initialize enclave.\n");
		return -1;
	}
	
	sgx_status_t internalStatus;
	sgx_status_t st=SGX_ERROR_INVALID_PARAMETER;
	uint8_t  result=SGX_EC_INVALID_SIGNATURE;
	st=ecall_get_users(global_eid, &internalStatus, 
		ListHRi,ListRi,initpublicKey,initprivateKey,
		ListSignature,TOTALUSERS-1);
	
	if(internalStatus!=SGX_SUCCESS||st!=SGX_SUCCESS)
	{
		printf("User's generation failed !!!\n");

	}
	/* Group key generation */
	int* GroupKey=(int*)malloc(sizeof(int)*REDUCERS);
	
	for (i=0;i<REDUCERS;i++)
	{
		GroupKey[i]=rand()%1000;
	}
	/* Simulated users' data generation*/
	for ( i=0; i<TOTALUSERS; i++) 
	UsersData[i] = (int *)malloc(2 * sizeof(int)); 

	for(i=0;i<TOTALUSERS;i++)
	{
		UsersData[i][0]=GroupKey[rand()%REDUCERS];
		UsersData[i][1]=rand()%5;
	}


	/* Logical manifest generation */
	cout<<"Logical manifest generation"<<endl;
	string  manifest="";
	// adding nb users to the manifest
	manifest+=to_string(TOTALUSERS)+"<=>"+to_string(MAPPERS)+"<=>"+to_string(REDUCERS);
	//adding dep
	for(i=0;i<REDUCERS;i++)
	{
		manifest+="<=>"+to_string(GroupKey[i]);
	}

	const char* t=manifest.c_str();
	
	sgx_ec256_public_t* AuthorPubKey=(sgx_ec256_public_t*)malloc(sizeof(sgx_ec256_public_t));;
	sgx_ec256_private_t* AuthorPrivKey=(sgx_ec256_private_t*)malloc(sizeof(sgx_ec256_private_t));
	sgx_ec256_signature_t* ManifestSignature=(sgx_ec256_signature_t*)malloc(sizeof(sgx_ec256_signature_t));
	
	//Authority's keypair generation
	st=ecall_generateKeys(global_eid, &internalStatus,AuthorPubKey,AuthorPrivKey);
	if(internalStatus!=SGX_SUCCESS||st!=SGX_SUCCESS)
	{
		printf("Authority's key generation failed!!!\n");

	}
	
	//Preparing manifest to be signed
	vector<uint8_t> myVector(manifest.begin(), manifest.end()+1);
	uint8_t *signableManifest = &myVector[0];
	//Manifest signature
	st=ecall_sign(global_eid, &internalStatus,signableManifest,manifest.length()*sizeof(uint8_t),*AuthorPrivKey,ManifestSignature);
	
	

	/* Not usefull, just to check*/
	st=ecall_verify_sign(global_eid,&internalStatus,signableManifest,manifest.length()*sizeof(uint8_t),*AuthorPubKey,*ManifestSignature,&result);
	if(result != SGX_EC_VALID)
	{
		cout<<"Signature pas ok"<<endl;
	}
	/******************************/




/*********************************/	
	sgx_destroy_enclave(global_eid);
/************************ End simulation init************************/
	set_client_id("123");
	tcp_connect("127.0.0.1", "1234");
	/** Begin untrusted driver *///
		cout<<"Launching TEE Monitor"<<endl;
		
		struct timeval t1, t2;
		double temps=0;
	for(i=0;i<MAXATTEMP;i++)
	{
		gettimeofday(&t1, NULL);
		
		cout<<"Connection to pubdb done. attemp: "<<i<<endl;
		
		//download the manifest
		if (get_manifest() == SUCCESS)
			cout<<"manifest downloaded"<<endl;
		else
			cout<<"error manifest"<<endl;
		
		// launching tee_monitor
		if (initialize_enclave_monitor(&global_eid) < 0) {
			fprintf(stderr, "Fail to initialize enclave.\n");
			return -1;
		}
		
		st=ecall_tee_monitor(global_eid,&internalStatus,t,  *ManifestSignature,  *AuthorPubKey, state )	;
		if(internalStatus!=SGX_SUCCESS||st!=SGX_SUCCESS)
		{
			printf("Launching tee monitor failed\n");
			print_error_message(st);

		}
		sgx_destroy_enclave(global_eid);		
		sgx_destroy_enclave(global_eid_op);
		gettimeofday(&t2, NULL);
		temps += (t2.tv_sec - t1.tv_sec) * 1000.0+(t2.tv_usec - t1.tv_usec)/1000 ; 



		
	}
	printf("temps = %lf\n", temps/MAXATTEMP);
	return 0;
}



static void set_client_id(char *cid)
{
  memcpy(client_id, cid, strlen(cid));
}

char *get_client_id(void)
{
  return client_id;
}

static void set_hash(char *hash)
{
  memset(sample_hash, 0, MAX_HASH_SIZE);
  memcpy(sample_hash, hash, strlen(hash));
}

char *get_hash(void)
{
  return sample_hash;
}




void ocall_print(const char* str) {
    cout<<str<<endl;
}
void ocall_print_key(sgx_key_128bit_t* str) {
	int i;
	cout<<"Key=";
   	for (i=0;i<16;i++)
	{
		cout<<+str[0][i];
	}
	cout<<endl;
}


void ocall_print_hash(sgx_sha256_hash_t* str) {
	int i;
	cout<<"Hash=";
   	for (i=0;i<SGX_SHA256_HASH_SIZE;i++)
	{
		cout<<+str[0][i];
	}
	cout<<endl;
}



void ocall_print_sign(sgx_ec256_signature_t* str) {
	int i;
	cout<<"x=";
   	for (i=0;i<SGX_NISTP_ECP256_KEY_SIZE;i++)
	{
		cout<<+str->x[i];
	}
	cout<<endl;
	cout<<"y=";
   	for (i=0;i<SGX_NISTP_ECP256_KEY_SIZE;i++)
	{
		cout<<+str->y[i];
	}
	cout<<endl;
}
void ocall_print_pubk(sgx_ec256_public_t* str) {
	int i;
	cout<<"Gx=";
   	for (i=0;i<SGX_ECP256_KEY_SIZE;i++)
	{
		cout<<+str->gx[i];
	}
	cout<<endl;
	cout<<"Gy=";
   	for (i=0;i<SGX_ECP256_KEY_SIZE;i++)
	{
		cout<<+str->gy[i];
	}
	cout<<endl;
}
void ocall_print_target(sgx_dh_msg1_t* str) {
	int i;
	cout<<"sgx_measurement_t=";
	
	for (i=0;i<SGX_HASH_SIZE;i++)
	{
		cout<<+str->target.mr_enclave.m[i];
	}
	cout<<endl;
	cout<<"sgx_attributes_t:(";
	cout<<"flags=";
	cout<<+str->target.attributes.flags<<";";
	cout<<"xfrm=";
	cout<<+str->target.attributes.xfrm<<")"<<endl;
}
void ocall_print_privk(sgx_ec256_private_t* str) {
	int i;
	cout<<"r=";
   	for (i=0;i<SGX_ECP256_KEY_SIZE;i++)
	{
		cout<<+str->r[i];
	}

	cout<<endl;
}
void ocall_print_dh_session(sgx_dh_session_t* str) {
	int i;
	cout<<"SGX_dh_session=";
   	for (i=0;i<SGX_DH_SESSION_DATA_SIZE;i++)
	{
		cout<<+str->sgx_dh_session[i];
	}

	cout<<endl;
}

void ocall_print_uint(uint32_t str) {
    cout<<+str<<endl;
}
void ocall_sendHRi(sgx_sha256_hash_t* HRi,sgx_ec256_public_t* PKi,sgx_ec256_signature_t* sign) {

	if (add_hash("123456781234567812345678123456781234567812345678123456781234569") == SUCCESS)
		if(LOG)
		cout<<"hash sent to QR"<<endl;
	else
		if(LOG)
		cout<<"error add_hash"<<endl;

	memcpy(&ListSignature[TOTALUSERS-1], sign, sizeof(sgx_ec256_signature_t));
	memcpy(&initpublicKey[TOTALUSERS-1], PKi, sizeof(sgx_ec256_public_t));
	memcpy(&ListHRi[TOTALUSERS-1], HRi, sizeof(sgx_sha256_hash_t));

}
void ocall_sendRi(uint32_t* Ri,sgx_ec256_public_t* PKi,sgx_ec256_signature_t* sign) {

	if (add_random("12")==SUCCESS)
		if(LOG)
		cout<<"Random number sent to QR"<<endl;
	else
		if(LOG)
		cout<<"error send random"<<endl;

	// memcpy(&ListSignature[TOTALUSERS-1], sign, sizeof(sgx_ec256_signature_t));
	// memcpy(&initpublicKey[TOTALUSERS-1], PKi, sizeof(sgx_ec256_public_t));
	memcpy(&ListRi[TOTALUSERS-1], Ri, sizeof(uint32_t));
}

void ocall_getListHRi(sgx_sha256_hash_t* LHRi,sgx_ec256_public_t* LPKi,sgx_ec256_signature_t* Lsign,
sgx_ec256_public_t* ChoosenpubKey, int N) {

	
	char hash_list[MAX_CLIENT_NUM][MAX_HASH_SIZE];
	int num_hash = get_hash_list(hash_list);
	if(LOG)
	cout<< "num hash = "<< num_hash <<endl;
	
	int j;
	// memcpy(ChoosenpubKey,&initpublicKey[rand()%N],sizeof(sgx_ec256_public_t));
	memcpy(ChoosenpubKey,&initpublicKey[TOTALUSERS-1],sizeof(sgx_ec256_public_t));
	for(j=0;j<N;j++)
	{	
		memcpy(&Lsign[j], &ListSignature[j], sizeof(sgx_ec256_signature_t));
		memcpy(&LPKi[j], &initpublicKey[j], sizeof(sgx_ec256_public_t));
		memcpy(&LHRi[j], &ListHRi[j], sizeof(sgx_sha256_hash_t));

	}


}
void ocall_getListRi(uint32_t* LRi, int N) {
	char random_list[MAX_CLIENT_NUM][MAX_RND_SIZE];
	get_random_list(random_list);
	int j;
	for(j=0;j<N;j++)
	{	
		memcpy(&LRi[j], &ListRi[j], sizeof(sgx_sha256_hash_t));

	}


}
void ocall_send_Xrand(uint32_t* Xrand,sgx_ec256_signature_t* signature)
{
	char c=(char)Xrand[0];
	add_rank(c);
	if(LOG)
	cout<<"Ranking random sent"<<endl;
	memcpy(&Xrandom, Xrand, sizeof(uint32_t));
	memcpy(&Xsignature, signature, sizeof(sgx_ec256_signature_t));
}
void ocall_get_Xrand(uint32_t* Xrand,sgx_ec256_signature_t* signature)
{
	char *selected_rank=(char*)malloc(sizeof(char));
	get_rank(selected_rank);
	if(LOG)
	cout<<"Ranking random received"<<endl;
	memcpy(Xrand, &Xrandom, sizeof(uint32_t));
	memcpy(signature, &Xsignature, sizeof(sgx_ec256_signature_t));

}
void ocall_launch()
{
	sgx_status_t internalStatus;
	sgx_status_t externalStatus;

	if (initialize_enclave_op(&global_eid_op) < 0) {
		fprintf(stderr, "Fail to initialize op enclave.\n");
	}


}
void ocall_send_tuple(char* message, char* output,size_t EncryptLength,size_t DecryptLength)
{
	sgx_status_t internalStatus;
	sgx_status_t externalStatus;
	// cout<<message<<endl;
	externalStatus=op_mapper(global_eid_op,&internalStatus,message,output,EncryptLength,DecryptLength);

}
void ocall_get_msg1_la(sgx_dh_msg1_t * msg1)
{
	sgx_status_t internalStatus;
	sgx_status_t externalStatus;
	
	externalStatus=op_get_msg1(global_eid_op,&internalStatus,msg1);

	if(externalStatus!=SGX_SUCCESS)
	{
		cout<<"Performing ecall_msg1 failed"<<endl;
		print_error_message(externalStatus);
	}
	if(internalStatus!=SGX_SUCCESS)
	{
		cout<<"Generating msg1 failed"<<endl;
		print_error_message(internalStatus);
	}
}
void ocall_get_msg3_la(sgx_dh_msg2_t * msg2, sgx_dh_msg3_t * msg3,sgx_dh_session_enclave_identity_t * initiator_identity)
{
	sgx_status_t internalStatus;
	sgx_status_t externalStatus;
	externalStatus=op_proc_msg2(global_eid_op,&internalStatus,msg2,msg3,initiator_identity);
	if(externalStatus!=SGX_SUCCESS)
	{
		cout<<"Performing ecall_msg1 failed"<<endl;
		print_error_message(externalStatus);
	}
	if(internalStatus!=SGX_SUCCESS)
	{
		cout<<"Generating msg1 failed"<<endl;
		print_error_message(internalStatus);
	}
}
void ocall_get_msg1_ra(sgx_dh_msg1_t * msg1)
{
	sgx_status_t internalStatus;
	sgx_status_t externalStatus;
	char* m1=(char*)malloc(sizeof(char)*256);
	sim_client_send("msg1");
	sim_client_receive(m1); 
	externalStatus=op_get_msg1(global_eid_op,&internalStatus,msg1);
	sim_client_send("msg1");
	sim_client_receive(m1); 
	
	if(externalStatus!=SGX_SUCCESS)
	{
		cout<<"Performing ecall_msg1 failed"<<endl;
		print_error_message(externalStatus);
	}
	if(internalStatus!=SGX_SUCCESS)
	{
		cout<<"Generating msg1 failed"<<endl;
		print_error_message(internalStatus);
	}
}
void ocall_send_result(char * msg1)
{
	sgx_status_t internalStatus;
	sgx_status_t externalStatus;
	char* m1=(char*)malloc(sizeof(char)*256);
	sim_client_send("msg1");
}
void ocall_get_msg3_ra(sgx_dh_msg2_t * msg2, sgx_dh_msg3_t * msg3,sgx_dh_session_enclave_identity_t * initiator_identity)
{
	sgx_status_t internalStatus;
	sgx_status_t externalStatus;
	char* m1=(char*)malloc(sizeof(char)*256);
	sim_client_send("msg2");
	sim_client_receive(m1); 
	externalStatus=op_proc_msg2(global_eid_op,&internalStatus,msg2,msg3,initiator_identity);
	sim_client_send("msg2");
	sim_client_receive(m1); 
	if(externalStatus!=SGX_SUCCESS)
	{
		cout<<"Performing ecall_msg1 failed"<<endl;
		print_error_message(externalStatus);
	}
	if(internalStatus!=SGX_SUCCESS)
	{
		cout<<"Generating msg1 failed"<<endl;
		print_error_message(internalStatus);
	}
}

string converter(uint8_t *str){
    return string((char *)str);
}
